#!/bin/bash
# mise a jour du contenu offline a partir du CMS
rm -rf www
wget -m -k -E -p -np https://videosub.fr/revocheck/
mv videosub.fr/revocheck/ www
rm -rf videosub.fr

# supprime les liens vers le site ...
# find www -name "*.html" -exec sed -i -e s/"<a href=\"https:\/\/videosub.fr\/biosub\">Accueil<\/a>"/"<a href=\"\/index.html\">Accueil<\/a>"/ {} \;

#pour ios on ajoute playsinline aux balises video ...
for fic in `find www -name *.html`
do
    sed -i -e s/"<video \(.*\) alt="/"<video \1 playsinline webkit-playsinline alt="/ ${fic}
    #rewrite external http links
    sed -i -e s/"<a href=\"https:\(.*\)\">"/"<a href=\"#\" onclick=\"window.open('https:\1', '_system'); return false;\">"/ ${fic}
done
