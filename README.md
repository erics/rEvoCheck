# Build with Cordova on debian 10:

`curl -sL https://deb.nodesource.com/setup_10.x | bash -`
`apt-get install -y nodejs`
`npm install -g cordova`

Install Android Studio

# Build instructions:

`export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/`
`export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/`
`cordova platform add android`
`cordova prepare android`
`cordova requirements android`
`cordova build andoid`
