#!/bin/bash
APPNAME="revocheck"
#pseudo script used to build package
export ANDROID_SDK_ROOT=/opt/android/sdk/
unset ANDROID_HOME
# export ANDROID_HOME=/opt/android/sdk/

export PATH=${PATH}:$ANDROID_SDK_ROOT:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/platform-tools:$ANDROID_SDK_ROOT/tools/bin/
SIGNID="ryxeo"
SIGFILE=~/bin/android_release.keystore

fullInstall=""

#par defaut on n'essaye pas de déployer via ADB
adbInstall="N"

function usage {
    echo "Usage: $(basename $0) [-n] [-f]" 2>&1
    echo "  -n : build leger"
    echo "  -d : build debug"
    echo "  -f : build complet avec suppression et réinstallation des plugins & base"
    echo "  -i : déploiement sur le téléphone via ADB à la fin du build"
    exit 1
}

optstring=":ndfhi"
while getopts ${optstring} arg; do
    case "${arg}" in
        h)
            usage
        ;;
        n)
            fullInstall="N"
        ;;
        d)
            debug="Y"
        ;;
        f)
            fullInstall="o"
        ;;
        i)
            echo "- on fait une installation via ADB à la fin ..."
            adbInstall="o"
        ;;
        *)
            usage
        ;;
    esac
done

if [[ ${#} -eq 0 ]]; then
    echo "Voulez vous faire un build complet avec suppression et réinstallation des plugins & base ?"
    echo -n "[o/N]"
    read fullInstall
fi

#dans tous les cas on fait place nette sinon par exemple le versionCode n'est pas mis à jour
rm -rf platforms/android/app/build/*

if [ "${fullInstall}" == "o" ]; then
    cordova platform remove android
    cordova platform add android@12.0.1
    
    # liste des plugins cordova pour la version android
    PLUGINS="" #cordova-plugin-image-cropper cordova-plugin-androidx cordova-plugin-androidx-adapter"
    for p in $PLUGINS
    do
        cordova plugin remove ${p}
    done
    PLUGINS="" #cordova-plugin-androidx-adapter phonegap-plugin-barcodescanner cordova-plugin-aes256-encryption"
    for p in $PLUGINS
    do
        cordova plugin add ${p}
    done
    
    #image cropper n'est pa publié chez npm
    #cordova plugin add https://github.com/Riyaz0001/cordova-plugin-image-cropper.git
fi

APPVERSION=`grep version config.xml | grep ${APPNAME} | sed s/".*version=\""/""/ | cut -d '"' -f1`
sed -i -e s%".*preference name=\"OverrideUserAgent\" value=\".*"%"    <preference name=\"OverrideUserAgent\" value=\"Mozilla/5.0 ${APPNAME}/${APPVERSION} Android\" />"% config.xml

if [ "${debug}" == "Y" ]; then
    cordova build android --debug
else
    cordova build android --release
fi

if [ ${?} -eq "0" ]; then
    echo "build ok ... la suite"
    if [ -z "${STOREPASS}" ]; then
        echo "Il manque la passphrase pour utiliser la clé de signature de l'app..."
        exit -2
    fi
    
    if [ "${debug}" == "Y" ]; then
        echo "no sign, debug mode"
        if [ "${adbInstall}" == "o" ]; then
            echo -n "Installation en cours via ADB ..."
            adb install -r platforms/android/app/build/outputs/apk/debug/app-debug.apk
        else
            echo "adbInstall = ${adbInstall} ... si vous voulez installer manuellement le paquet :"
            echo "adb install -r platforms/android/app/build/outputs/apk/debug/app-debug.apk"
        fi
    else
        OUTFILE="./platforms/android/app/build/outputs/bundle/release/app-release.aab"
        OUTAPK="./platforms/android/app/build/outputs/apk/release/app-release.apks"
        jarsigner -verbose -sigalg SHA256withRSA -digestalg SHA-256 -keystore ${SIGFILE} -storepass ${STOREPASS} ${OUTFILE} ${SIGNID}
        
        if [ ${?} -eq "0" ]; then
            echo "sign ok ... la suite"
            java -jar /opt/android/bundletool-all-1.8.0.jar build-apks --bundle=${OUTFILE}  \
            --output=${OUTAPK} --ks=${SIGFILE} --ks-key-alias=${SIGNID} --ks-pass=pass:${STOREPASS}
            
            #copy file to /tmp
            TMPFILENAME="/tmp/${APPNAME}-${APPVERSION}.aab"
            echo "Copie du paquet vers ${TMPFILENAME}"
            cp ${OUTFILE} ${TMPFILENAME}
            
            if [ "${adbInstall}" == "o" ]; then
                echo -n "Installation en cours via bundletool ..."
                java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}
            else
                echo "Si vous voulez installer manuellement le paquet :"
                echo "java -jar /opt/android/bundletool-all-1.8.0.jar install-apks --apks=${OUTAPK}"
            fi
            
        else
            echo "erreur de signature"
            exit -2
        fi
    fi
else
    echo "erreur"
    exit -1
fi

